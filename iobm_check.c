
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <inttypes.h> 

#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

#include "commonFunctions.h"
//#include "memoryCheck.h"
#include "global.h"



//Size of one memory page in bytes.
#define PAGESIZE 4096

uint32_t iobmArray[(2*PAGESIZE)/32];

int iobmCheck(int argc, char *argv[])
{
	


	xmlDocPtr Bpolicydoc;
    xmlXPathContextPtr BxpathCtx; 

    //printf("reached iobmCheck\n");

    /* Load XML document */
    Bpolicydoc = xmlParseFile(argv[2]);
    if (Bpolicydoc == NULL) {
    	fprintf(stderr, "Error: unable to parse file \"%s\"\n", argv[2]);
    	return -1;
    }

    /* Create xpath evaluation context */
    BxpathCtx = xmlXPathNewContext(Bpolicydoc);
    if(BxpathCtx == NULL) {
        fprintf(stderr,"Error: unable to create new XPath context\n");
        xmlFreeDoc(Bpolicydoc); 
        return -1;
    }

    FILE* iobmFile;

    xmlChar* subjectsExpr;
    xmlChar* BxpathExprMem;
    xmlChar* BxpathExprPort;
    xmlChar* BxpathExprDevice;
    xmlChar* BxpathExprIoport;
    xmlChar* BxpathExprIoportPhy;
   
    xmlXPathObjectPtr subjectsObj, BxpathObjMem, BxpathObjPort, BxpathObjDevice, BxpathObjIoport ;
    xmlXPathObjectPtr BxpathObjIoportPhy;
    xmlNodeSetPtr BsubjectsElement;
    xmlNodeSetPtr BFileElement;
    xmlNodeSetPtr BDeviceElement,BDeviceElements, BIoportElements, BIoportElementsPhy;
   

    int numberOfSubjects, numberOfDevices, numberOfIoports;




    subjectsExpr="/system/subjects/subject[@id]";
    subjectsObj = xmlXPathEvalExpression(subjectsExpr, BxpathCtx);

    if(subjectsObj == NULL) {
        fprintf(stderr,"Error: unable to evaluate xpath expression \"%s\"\n", subjectsExpr);
        xmlXPathFreeContext(BxpathCtx); 
        xmlFreeDoc(Bpolicydoc); 
        return -1;
    }

    BsubjectsElement= subjectsObj->nodesetval;    
    
    numberOfSubjects =(BsubjectsElement) ? BsubjectsElement->nodeNr : 0;
    //printf("Number of Subjects = %d \n", numberOfSubjects);


    char subjectName[50], tempString[100], fileName[50], DeviceName[50], IoportName[50];
    char startAddr[50], endAddr[50], DeviceNamePhy[50];
    uint64_t startAddrDec, endAddrDec;

    for(int i = 0; i < numberOfSubjects; ++i) { 


    	for (int z = 0; z < (2*PAGESIZE)/32; ++z)
    	{
    		iobmArray[z] = 0xffffffff;
    	}

    	// for (int z = 0; z < (2*PAGESIZE)/32; ++z)
    	// {
    	// 	//printf("iobmArray[%d]= %d \n",z, TestBit(iobmArray, z));
    	// 	printf("iobmArray[%d]= %x ",z, iobmArray[z]);
    	// }

    	// printf("\n");printf("\n");printf("\n");

    	// ClearBit(iobmArray,0);

    	// for (int z = 0; z < 10; ++z)
    	// {
    	// 	//printf("iobmArray[%d]= %d \n",z, TestBit(iobmArray, z));
    	// 	printf("iobmArray[%d]= %x ",z, iobmArray[z]);
    	// }

    	// printf("iobmArray[1]= %d \n",TestBit(iobmArray, 0));

    	//printf("\n\n\n");



    	strcpy(subjectName,  xmlGetProp(BsubjectsElement->nodeTab[i], "name") );  
        printf("subject name = %s\n", subjectName );



        sprintf(tempString, "/system/memory/memory[@name='%s|iobm']/file", subjectName);
        BxpathExprMem = tempString;

        //printf("BxpathExprMem = %s\n", tempString );

        /*Execute expression*/
   		BxpathObjMem = xmlXPathEvalExpression(BxpathExprMem, BxpathCtx);

    	/*Validate  xpathobj*/
    	if(BxpathObjMem == NULL) {
        	fprintf(stderr,"Error: unable to evaluate xpath expression \"%s\"\n", BxpathExprMem);
        	xmlXPathFreeContext(BxpathCtx); 
        	xmlFreeDoc(Bpolicydoc); 
        	return -1;
    	}

     
    	BFileElement = BxpathObjMem->nodesetval;
    	strcpy(fileName,  xmlGetProp(BFileElement->nodeTab[0], "filename"));
    	printf("fileName = %s \n", fileName );
    	// strcpy(fileOffset,  xmlGetProp(tempBFileElement->nodeTab[0], "offset"));



    	sprintf(tempString, "/system/subjects/subject[@id='%d']/devices/device", i);
    	BxpathExprDevice = tempString;

    	/*Execute expression*/
   		BxpathObjDevice = xmlXPathEvalExpression(BxpathExprDevice, BxpathCtx);

    	/*Validate  xpathobj*/
    	if(BxpathObjDevice == NULL) {
        	fprintf(stderr,"Error: unable to evaluate xpath expression \"%s\"\n", BxpathExprDevice);
        	xmlXPathFreeContext(BxpathCtx); 
        	xmlFreeDoc(Bpolicydoc); 
        	return -1;
    	}


    	BDeviceElements= BxpathObjDevice->nodesetval;    
    
    	numberOfDevices =(BDeviceElements) ? BDeviceElements->nodeNr : 0;
    	printf("Number of Devices = %d \n", numberOfDevices);

    	if(numberOfDevices > 0){
    		for (int j = 0; j < numberOfDevices; ++j)
    		{
    			//BDeviceElement = BDeviceElements->nodesetval;
    			strcpy(DeviceName,  xmlGetProp(BDeviceElements->nodeTab[j], "logical"));
    			printf("DeviceName = %s \n", DeviceName );

    			strcpy(DeviceNamePhy,  xmlGetProp(BDeviceElements->nodeTab[j], "physical"));
    			printf("DeviceNamePhysical = %s \n", DeviceNamePhy );

    			sprintf(tempString, "/system/subjects/subject[@id='%d']/devices/device[@logical='%s']/ioPort", i,DeviceName);
    			BxpathExprIoport = tempString;

    			//printf("IOPORT = %s \n", tempString );


		    			/*Execute expression*/
		   		BxpathObjIoport = xmlXPathEvalExpression(BxpathExprIoport, BxpathCtx);

		    	/*Validate  xpathobj*/
		    	if(BxpathObjIoport == NULL) {
		        	fprintf(stderr,"Error: unable to evaluate xpath expression \"%s\"\n", BxpathExprIoport);
		        	xmlXPathFreeContext(BxpathCtx); 
		        	xmlFreeDoc(Bpolicydoc); 
		        	return -1;
		    	}


		    	BIoportElements= BxpathObjIoport->nodesetval;    
		    
		    	numberOfIoports =(BIoportElements) ? BIoportElements->nodeNr : 0;
		    	printf("Number of Ioports = %d \n", numberOfIoports);

		    	if(numberOfIoports > 0){
		    		for (int k = 0; k < numberOfIoports; ++k)
		    		{
		    			strcpy(IoportName,  xmlGetProp(BIoportElements->nodeTab[k], "physical"));
    					printf("IoportName = %s \n", IoportName );

    					sprintf(tempString, "/system/hardware/devices/device[@name='%s']/ioPort[@name='%s']",DeviceNamePhy, IoportName);
    					BxpathExprIoportPhy = tempString;

    					//printf("BxpathExprIoportPhy = %s \n", tempString );

    					BxpathObjIoportPhy = xmlXPathEvalExpression(BxpathExprIoportPhy, BxpathCtx);

		    			/*Validate  xpathobj*/
				    	if(BxpathObjIoportPhy == NULL) {
				        	fprintf(stderr,"Error: unable to evaluate xpath expression \"%s\"\n", BxpathExprIoportPhy);
				        	xmlXPathFreeContext(BxpathCtx); 
				        	xmlFreeDoc(Bpolicydoc); 
				        	return -1;
				    	}


		    			BIoportElementsPhy= BxpathObjIoportPhy->nodesetval; 

		    			strcpy(startAddr,  xmlGetProp(BIoportElementsPhy->nodeTab[0], "start"));
		    			strcpy(endAddr,  xmlGetProp(BIoportElementsPhy->nodeTab[0], "end"));

    					printf("startAddr = %s \n", startAddr );
    					printf("endAddr = %s \n", endAddr );

    					formatHex(startAddr);
        				startAddrDec = convtodecnum(startAddr);

        				formatHex(endAddr);
        				endAddrDec = convtodecnum(endAddr);

        				printf("startAddrDec(%" PRIu64 ")", startAddrDec);
        				printf("endAddrDec(%" PRIu64 ")\n", endAddrDec);

        				// for (int l = startAddrDec-2; l < endAddrDec+2; ++l)
        				// {
        					
        				// 	printf("iobmArray[%d]= %d \n",l, TestBit(iobmArray, l));
        				// }

        				for (int l = startAddrDec; l <= endAddrDec; ++l)
        				{
        					//iobmArray[l] = 0;
        					ClearBit(iobmArray, l);
        				}



        				// for (int l = startAddrDec-2; l < endAddrDec+2; ++l)
        				// {
        					
        				// 	printf("iobmArray[%d]= %d \n",l, TestBit(iobmArray, l));
        				// }
        				


        				printf("\n\n");
        				//printf("sizeof int = %lu\n", sizeof(int) );
        				//exit(0);
		    		}
		    	}
    		}//end of devices loop

    		char iobmFileName[50];
    		sprintf(iobmFileName, "files/processedFiles/%s_V2.txt", fileName);
    		printf("fileName === %s\n",iobmFileName);
    		iobmFile = fopen(iobmFileName, "r");
			if (iobmFile == NULL){
	         	printf("Cannot open iobmFile file.  \n");
			 	exit(0);
	     	}

	     	char c1,char1,char2;
	     	uint32_t curValue = 0;
    		for(int x=0; x < (PAGESIZE*2)/32; x = x+1){
	     		//printf("%x \n",  iobmArray[x]);
	     		if(iobmArray[x] == 0xffffffff){
	     			for(int p=0; p<8;p++){
	     				if(fgetc(iobmFile) != 'f'){
	     					printf("IOBM file contains non 'f' character at x= %d\n",x);
	    					CheckStopOnError();
	     				}
	     			}
	     			

	     		}else{
	     			printf("non 0xffffffff found in iobmArray\n");
	     			//printf("%x \n",  iobmArray[x]);
	     			curValue = iobmArray[x];
	     			int flag = 0;
	     			for(int p=0; p<8;p++){
	     				flag = 0;
	     				int tempVal1 = ((curValue & (0xf<< (p*4)))>>(p*4));
	     				p = p+1;
	     				int tempVal2 = ((curValue & (0xf<< (p*4)))>>(p*4));
	     				char1 = fgetc(iobmFile);
	     				char2 = fgetc(iobmFile);



	     				for(int v=0;v<2; v++){
	     					//printf(" character = %c\n", char1 );
		     				switch(char1){
		     					case '0' : if(tempVal2 != 0){ flag =1;}break;
		     					case '1' : if(tempVal2 != 1){ flag =1;}break;
		     					case '2' : if(tempVal2 != 2){ flag =1;}break;
		     					case '3' : if(tempVal2 != 3){ flag =1;}break;
		     					case '4' : if(tempVal2 != 4){ flag =1;}break;
		     					case '5' : if(tempVal2 != 5){ flag =1;}break;
		     					case '6' : if(tempVal2 != 6){ flag =1;}break;
		     					case '7' : if(tempVal2 != 7){ flag =1;}break;
		     					case '8' : if(tempVal2 != 8){ flag =1;}break;
		     					case '9' : if(tempVal2 != 9){ flag =1;}break;
		     					case 'a' : if(tempVal2 != 0xa){ flag =1;}break;
		     					case 'b' : if(tempVal2 != 0xb){ flag =1;}break;
		     					case 'c' : if(tempVal2 != 0xc){ flag =1;}break;
		     					case 'd' : if(tempVal2 != 0xd){ flag =1;}break;
		     					case 'e' : if(tempVal2 != 0xe){ flag =1;}break;
		     					case 'f' : if(tempVal2 != 0xf){ flag =1;}break;

		     				}

		     				if(flag ==1){
		     					printf("IOBM file contains non 'f' character at x= %d, p=%d \n",x,p);
		     					CheckStopOnError();
		     				}

		     				char1 = char2;
		     				tempVal2 =tempVal1;
	     				}
	     				
	     		
	     			}
	     			
	     		}
    			
    		}

    		//printf("%x\n",  TestBit(iobmArray, 905));
    		//exit(0);



    	}else{
    		//number of devices = 0
    		char iobmFileName[50];
    		sprintf(iobmFileName, "files/processedFiles/%s_V2.txt", fileName);
    		printf("fileName === %s\n",iobmFileName);
    		iobmFile = fopen(iobmFileName, "r");
			if (iobmFile == NULL){
	         	printf("Cannot open iobmFile file.  \n");
			 	exit(0);
	     	}


	     	char charF = 'f';
	     	char c1;
    		
    	
    	
	    	for (int y =0; y < PAGESIZE*2*2; ++y){	

	    		if((c1 = fgetc(iobmFile))!= EOF){
	    			
	    		
	    			if(c1 != charF){
	    				printf("IOBM file contains non 'f' character\n");
	    				CheckStopOnError();
	    			}
	    		}else{
	    			//printf("breaking\n");
	    			//break;
	    			printf("end of file reached\n");
	    			CheckStopOnError();
	    		}
	    	}

	    	printf("iobm check finished\n");


    	}



    	printf("\n\n\n");

    }



	xmlXPathFreeContext(BxpathCtx); 
    xmlFreeDoc(Bpolicydoc); 

    /* Shutdown libxml */
    xmlCleanupParser();
	


    printf("reached iobmCheck end\n");
	return 0;
}
