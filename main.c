
#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h> 

#include "commonFunctions.h"
#include "memoryCheck.h"
#include "imageChecker.h"
#include "ds_checker.h"
#include "iobm_check.h"
#include "global.h"

 int GnoOfSubjects;
 int GnoOfCPUs;
 int GnoOfActiveCPUs;

 uint64_t GtotalKernelMemorySize;
 uint64_t GtotalMemorySize;
 uint64_t GtotalFileElementSize;
 uint64_t GtotalFillElementSize;
 int stopOnError;
int	main(int argc, char* argv[])
{
	/* code */
	if(argc < 3){
		printf("usage: ./mainTool usrePolicy BPolicy ");

	}

	//printf("argc = %d", argc);
	stopOnError = 0;
	if(argc == 4){
		stopOnError = strtol(argv[3], NULL, 10); 
		//printf("StopOnError %d\n", stopOnError);
	}

	printf("\nMemory Check Started.\n");
	memoryCheck(argc, argv);
	printf("\nMemory Check Finished.\n");
	printf("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
	printf("\nImage Check Started.\n");
	imageCheck(argc, argv);
	printf("\nImage Check Finished.\n");
	printf("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
	printf("\nMuen data structures Check Started.\n");
	ds_check(argc, argv);
	printf("\nDS Check Finished.\n");

	printf("\n\n*****************************************************************************\n");
	printf("\nIobm Check Started.\n");
	iobmCheck(argc, argv);
	printf("\nIobm Check Finished.\n");

	printf("\n\n*****************************************************************************\n");
	printf("#Subjects  #CPUs  #ActiveCPUs  TotalMemorySize  KernelMemorySize  FileElementsSize  FillElementsSize\n");
	printf("%d\t\t%d\t%d",GnoOfSubjects, GnoOfCPUs, GnoOfActiveCPUs );
	
	printf("\t%" PRIu64 "\t", GtotalMemorySize );
	printf("%" PRIu64 "\t\t\t", GtotalKernelMemorySize );
	printf("%" PRIu64 "\t", GtotalFileElementSize );
	printf("%" PRIu64 "", GtotalFillElementSize );


    printf("\n*****************************************************************************\n\n");


	
	return 0;
}
